# Test_SkillRate

Этот проект реализует API для взаимодействия с БД для хранения тестов

# test_skillRate

Веб-приложение оценивания студентов с учётом состава тем дисциплин

[фигма](https://www.figma.com/file/Ft5sZ5MfDSfHqif1MamZXS/SkillRate?type=design&node-id=249-2775&mode=design&t=3rZghXe68igQkxXv-0)

## Быстрый старт

Запуск БД Mongo и приложения test_skillRate

```bash
make
```

Остановка БД Mongo и приложения test_skillRate

```bash
make stop
```

Построение Docker образов для сервисов

```bash
docker compose -f docker-compose.yml build
```

Запуск приложения через Docker compose

```bash
docker compose -f docker-compose.yml up
```

## Описание файлов и директорий

- `app`: Директория с модулями FastAPI приложения
    - `api`: Директория с файлами, отвечающими за API маршруты(роуты)
        - `tests_handler.py`: Файл с маршрутами(роутами)
    - `crud`: Директория с функциями для выполнения операций CRUD в БД
        - `test_crud.py`: Функции для выполнения операций CRUD в БД
    - `database`: Директория с моделями и скриптами для работы с базой данных
        - `database.py`: Файл описывающий модели для БД
        - `models.py`: Файл описывающий модели для БД
    - `main.py`: Основной файл FastAPI приложения
    - `templates`: Содержит HTML шаблоны для веб-страниц
        - `index.html`: HTML шаблон для главной страницы приложения
- `docker-compose.yml`: Файл для конфигурации Docker сompose
- `Dockerfile`: Файл для создания Docker-образа приложения
- `Makefile`: Файл для автоматизации сборки и запуска приложения
- `README.md`: Этот файл, содержащий описание проекта и инструкции по его запуску
- `requirements.txt`: Файл со списком зависимостей
- `.env` - Файл с конфигурационными переменными: секретные ключи, настройки базы данных