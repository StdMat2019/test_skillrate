import json
import datetime
import random

import httpx
import numpy as np
from fastapi import FastAPI, UploadFile, File, Form
from fastapi import Request
from fastapi.templating import Jinja2Templates
from loguru import logger

from .api.tests_handler import quizzer
from .crud.test_crud import QuestionsCRUD
from .crud.test_crud import SkillsParamCRUD
from .crud.test_crud import TestParamCRUD
from .database.database import get_collection
from .database.database import get_database
from .database.models import Questions
from .database.models import Skills_param
from .database.models import Test_param

app = FastAPI()
app.include_router(quizzer, prefix="/quizzer", tags=["quizzer"])

templates = Jinja2Templates(directory="app/templates")


async def send_post_request(collection_name: str, course_fullname: str, skills_dict: dict, is_open: bool = False):
    async with httpx.AsyncClient() as client:
        url = "http://skillrate_backend:8001/admin/add_test_group_course_mongo"
        payload = {
            "collection_name": collection_name,
            "test_fullname": course_fullname,
            "is_open": str(is_open),
            "skills_dict": json.dumps(skills_dict)
        }
        response = await client.post(url, data=payload)
        return response.text


@app.get("/")
async def upload_file(request: Request):
    return templates.TemplateResponse("index.html", {"request": request})


async def parse_file(content_str, file_name: str):
    # Переменные для конфига
    determiner = '#'
    default_time_to_test = datetime.timedelta(minutes=10)
    default_test_name = file_name  # на входе название в формате ПРЕДМЕТ.НАЗВАНИЕ_ТЕСТА (python.pz1)
    default_count_questions = 10
    test_fullname = file_name
    default_weight = 1
    test_name = "название"
    test_time = "время"
    test_count_questions = "число"
    skills_dict = {}
    index = 0
    content = content_str.split("\n")

    # logger.info(determiner)
    # logger.info(content[index])
    while determiner not in content[index]:
        # logger.info(content[index].split(":"))
        if test_name in content[index].lower():
            test_fullname = content[index].split(":")[1].strip()
        if test_time in content[index].lower():
            default_time_to_test = datetime.timedelta(minutes=int(content[index].split(":")[1].strip()))
        if test_count_questions in content[index].lower():
            default_count_questions = int(content[index].split(":")[1].strip())
        index += 1
    db = get_collection(default_test_name, get_database("skillrate_quizzer"))  # в переменную

    while index + 3 < len(content):
        res = []
        true_res = []
        if determiner in content[index]:
            skill_name = content[index].split(":")[0].replace('#', '').strip()
            logger.info(content[index].split(":"))
            default_weight = content[index].split(":")[1]

            index += 1
            title = content[index].strip()
            index += 1
            while determiner not in content[index] and index + 1 < len(content):
                if content[index] != '':
                    res_ans = content[index].strip()[1:].strip()  # убираем символ отвечающий за правильность ответа
                    if content[index].strip().split(" ")[0] in ["+", "*"]:
                        true_res.append(res_ans)
                        # добавить для парсинга вопросов где необходимо установить последовательность
                    if res_ans != '':
                        res.append(res_ans)
                index += 1
            quest_param = Questions(skill=skill_name, weight=default_weight, header=title,
                                    answers_dict=res, true_answers_dict=true_res, type='выбор ответа',
                                    type_doc='question').get_dict()
            logger.info(quest_param)
            quest_crud = QuestionsCRUD()

            await quest_crud.create_question(quest_param, db)
            if skill_name in skills_dict:
                skills_dict[skill_name] += 1
            else:
                skills_dict[skill_name] = 1
        else:
            if index + 1 < len(content):
                index += 1
    test_param = Test_param(amount_question=default_count_questions, test_fullname=test_fullname,
                            time=default_time_to_test.total_seconds(), type_doc='test_param').get_dict()

    # logger.info(test_param)
    skills_param = Skills_param(skills=skills_dict, type_doc='skills_param').get_dict()
    # logger.info(skills_param)
    test_crud = TestParamCRUD()
    skill_crud = SkillsParamCRUD()
    await test_crud.create_test_param(test_param, db)
    await skill_crud.create_skills_param(skills_param, db)
    logger.info(await send_post_request(default_test_name, test_fullname, skills_dict))
    return file_name


@app.post("/")
async def parse_upload_file(request: Request, file_name: str = Form(...), file: UploadFile = File(...)):
    content = await file.read()
    content_str = content.decode()
    await parse_file(content_str, file_name)


@app.post("/parse_upload_file")
async def parse_upload_file(request: Request, tests_file: list):
    logger.info(tests_file)
    await parse_file(tests_file[1], tests_file[0])


def generate_id_next_skill(skills_distribution: dict):
    ids = list(skills_distribution.keys())
    distribution = list(skills_distribution.values())
    p = np.array(distribution) / sum(distribution)
    number = np.random.choice(ids, p=p)
    return number


@app.post("/get_test")
async def create_test(skills_dict_list: list):  # список из 3 элементов: названия теста и предмета(python.pz1),
    # словарь навыков студента, словарь название навыка и id
    logger.info(skills_dict_list)
    limit_get_quest = 0.6  # если процент балла за тест больше порога, то тест не выдается
    crud_param = SkillsParamCRUD()
    crud_tests_param = TestParamCRUD()
    db = get_database("skillrate_quizzer")  # в переменную
    db = get_collection(skills_dict_list[0], db)
    res = await crud_param.read_skills_param(db)  # получение распределения навыков в тесте
    logger.info(res)
    test_params = await crud_tests_param.read_test_param(db)  # параметры теста время и число вопросов
    logger.info("get_test")
    # обработать случай когда скилл из теста не существует в БД
    del_skills = []
    # удалить навыки которых нет в тесте
    for skill in skills_dict_list[1]:
        if skill not in res['skills'].keys():
            del_skills.append(skill)
    for skill in del_skills:
        del skills_dict_list[1][skill]
    # добавление навыков которые есть в тесте, но нет у студента
    for skill in res['skills'].keys():
        if skill not in skills_dict_list[1].keys():
            skills_dict_list[1][skill] = 1
    asked_questions = skills_dict_list[3]
    crud_question = QuestionsCRUD()
    test_questions_id = []
    num_question = 1
    task_counter = 0  # Для проверки на то что вопросов для выбора больше чем число вопросов для теста
    logger.info("sdfgjsdlkfjsldjflskd")
    logger.info(skills_dict_list[1])
    while num_question <= test_params["amount_question"]:
        skill_name = generate_id_next_skill(skills_dict_list[1])  # генерация навыка
        quest_obj = await crud_question.read_questions_by_skill(skill_name, db)
        question_id = random.choice(quest_obj)["question_id"]
        # вопрос задавался
        if question_id in asked_questions.keys():
            # результат ниже лимита и вопрос не в списке
            if (asked_questions[question_id][1] / asked_questions[question_id][0] < limit_get_quest
                    and question_id not in test_questions_id):
                test_questions_id.append(question_id)
                num_question += 1
                # Тут нужно сделать изменение флага old_result в таблице QuestStat на True
                # Тк вопрос отвеченный неправильно задался еще раз

        # вопрос не задавался
        else:
            # вопрос не в списке
            if question_id not in test_questions_id:
                test_questions_id.append(question_id)
                num_question += 1
        task_counter += 1
        if task_counter > 3 * test_params["amount_question"]:
            # Если нет столько неизвестных вопросов
            return None
    crud_quest = QuestionsCRUD()
    test_questions = []
    for question_id in test_questions_id:
        res = await crud_quest.read_question(question_id, db)
        del res["_id"]
        del res["true_answers_dict"]
        del res["type_doc"]
        test_questions.append(res)
    test_questions[0]["test_fullname"] = test_params["test_fullname"]
    test_questions[0]["time"] = test_params["time"]
    return test_questions


@app.post("/check_results")
async def check_results(answers_list: list):
    logger.info("check_results")
    db = get_database("skillrate_quizzer")  # в переменную
    db = get_collection(answers_list[2], db)
    id_answers_dict = answers_list[0]
    questions_id_list = answers_list[1]
    crud_quest = QuestionsCRUD()
    questions = await crud_quest.read_questions_by_id(questions_id_list, db)
    total_quest_score = 0
    total_res_quest = 0
    question_score_dict = {}
    for question in questions:
        res_question = 0
        if question["question_id"] in id_answers_dict:
            true_res = set(question["true_answers_dict"]) & set(id_answers_dict[question["question_id"]])
            false_res = set(id_answers_dict[question["question_id"]]) - set(question["true_answers_dict"])
            if (len(true_res) - len(false_res)) > 0 and len(question["true_answers_dict"]) != 0:
                res_question = round((((len(true_res) - len(false_res)) / len(question["true_answers_dict"])) *
                                      int(question["weight"])), 1)
                total_res_quest += res_question
        # Обработка вопроса без правильного ответа
        else:
            if len(question["true_answers_dict"]) == 0:
                res_question = int(question["weight"])
                total_res_quest += res_question
        total_quest_score += int(question["weight"])

        question_score_dict[question["question_id"]] = res_question, int(question["weight"]), question["header"]
    # Запись результатов о тесте производится в skillrate
    return {"total_res_quest": total_res_quest, "total_quest_score": total_quest_score,
            "question_score_dict": question_score_dict}


@app.post("/get_questions")
async def get_questions(questions_list: list):
    logger.info(questions_list)
    db = get_database("skillrate_quizzer")  # в переменную
    db = get_collection(questions_list[0], db)
    crud_quest = QuestionsCRUD()
    crud_tests_param = TestParamCRUD()
    questions = await crud_quest.read_questions_by_id(questions_list[1], db)
    test_params = await crud_tests_param.read_test_param(db)  # параметры теста время и число вопросов
    for question in questions:
        del question["_id"]
        del question["true_answers_dict"]
        del question["type_doc"]
    questions[0]["test_fullname"] = test_params["test_fullname"]
    logger.info(questions)
    return questions
