from fastapi import APIRouter
from fastapi import Depends, HTTPException
from fastapi.templating import Jinja2Templates
from motor.motor_asyncio import AsyncIOMotorCollection

from ..crud.test_crud import QuestionsCRUD
from ..crud.test_crud import SkillsParamCRUD
from ..crud.test_crud import TestParamCRUD
from ..database.database import get_collection

quizzer = APIRouter()
# quizzer.mount("/pages", StaticFiles(directory="/app/frontend/src"), name="pages")
templates = Jinja2Templates(directory="/app/frontend/src/")

test_param_crud = TestParamCRUD()
skills_param_crud = SkillsParamCRUD()
questions_crud = QuestionsCRUD()


@quizzer.get("/read_skills_param/{skill_param_id}")
async def read_skills_param(skill_param_id: str, db: AsyncIOMotorCollection = Depends(get_collection)):
    result = await skills_param_crud.read_skills_param(skill_param_id, db)
    if result:
        return result
    else:
        raise HTTPException(status_code=404, detail="Skills Param not found")


@quizzer.get("/read_test_param/{test_param_id}")
async def read_test_param(test_param_id: str):
    result = await test_param_crud.read_test_param(test_param_id)
    if result:
        return result
    else:
        raise HTTPException(status_code=404, detail="Test Param not found")


@quizzer.get("/read_question/{question_id}")
async def read_question(question_id: str, db: AsyncIOMotorCollection = Depends(get_collection)):
    result = await questions_crud.read_question(question_id, db)
    if result:
        return result
    else:
        raise HTTPException(status_code=404, detail="Question not found")


@quizzer.post("/create_test_param")
async def create_test_param(test_param_data: dict, connection: AsyncIOMotorCollection = Depends(get_collection)):
    try:
        result = await test_param_crud.create_test_param(test_param_data, connection)
        return {"message": f"Test Param created with ID: {result['inserted_id']}"}
    except ValueError as ve:
        raise HTTPException(status_code=400, detail=str(ve))


@quizzer.post("/create_skills_param")
async def create_skills_param(skills_param_data: dict, db: AsyncIOMotorCollection = Depends(get_collection)):
    try:
        result = await skills_param_crud.create_skills_param(skills_param_data, db)
        return {"message": f"Skills Param created with ID: {result['inserted_id']}"}
    except ValueError as ve:
        raise HTTPException(status_code=400, detail=str(ve))


@quizzer.post("/create_question")
async def create_question(question_data: dict, db: AsyncIOMotorCollection = Depends(get_collection)):
    try:
        result = await questions_crud.create_question(question_data, db)
        return {"message": f"Question created with ID: {result['inserted_id']}"}
    except ValueError as ve:
        raise HTTPException(status_code=400, detail=str(ve))


@quizzer.put("/update_test_param/{test_param_id}")
async def update_test_param(test_param_id: str, new_data: dict):
    result = await test_param_crud.update_test_param(test_param_id, new_data)
    if result["modified_count"] > 0:
        return {"message": f"Test Param with ID {test_param_id} updated successfully"}
    else:
        raise HTTPException(status_code=404, detail="Test Param not found")


@quizzer.put("/update_skills_param/{skill_param_id}")
async def update_skills_param(skill_param_id: str, new_data: dict,
                              db: AsyncIOMotorCollection = Depends(get_collection)):
    result = await skills_param_crud.update_skills_param(skill_param_id, new_data, db)
    if result["modified_count"] > 0:
        return {"message": f"Skills Param with ID {skill_param_id} updated successfully"}
    else:
        raise HTTPException(status_code=404, detail="Skills Param not found")


@quizzer.put("/update_question/{question_id}")
async def update_question(question_id: str, new_data: dict, db: AsyncIOMotorCollection = Depends(get_collection)):
    result = await questions_crud.update_question(question_id, new_data, db)
    if result["modified_count"] > 0:
        return {"message": f"Question with ID {question_id} updated successfully"}
    else:
        raise HTTPException(status_code=404, detail="Question not found")


@quizzer.delete("/delete_test_param/{test_param_id}")
async def delete_test_param(test_param_id: str):
    result = await test_param_crud.delete_test_param(test_param_id)
    if result["deleted_count"] > 0:
        return {"message": f"Test Param with ID {test_param_id} deleted successfully"}
    else:
        raise HTTPException(status_code=404, detail="Test Param not found")


@quizzer.delete("/delete_skills_param/{skill_param_id}")
async def delete_skills_param(skill_param_id: str, db: AsyncIOMotorCollection = Depends(get_collection)):
    result = await skills_param_crud.delete_skills_param(skill_param_id, db)
    if result["deleted_count"] > 0:
        return {"message": f"Skills Param with ID {skill_param_id} deleted successfully"}
    else:
        raise HTTPException(status_code=404, detail="Skills Param not found")


@quizzer.delete("/delete_question/{question_id}")
async def delete_question(question_id: str, db: AsyncIOMotorCollection = Depends(get_collection)):
    result = await questions_crud.delete_question(question_id, db)
    if result["deleted_count"] > 0:
        return {"message": f"Question with ID {question_id} deleted successfully"}
    else:
        raise HTTPException(status_code=404, detail="Question not found")
