import uuid

from sqlalchemy import Column, Integer, String, Date, ForeignKey, Float
from sqlalchemy.dialects.postgresql import JSON
from sqlalchemy.orm import DeclarativeBase
from sqlalchemy_utils import UUIDType


class Base(DeclarativeBase):
    pass


class Test_param(Base):
    __tablename__ = 'test_param'
    type_doc = Column(String(15), nullable=False, default="test_param")
    test_param_id = Column(UUIDType, primary_key=True, default=uuid.uuid4)
    test_fullname = Column(String(60))
    amount_question = Column(Integer, nullable=True)
    time = Column(String(30))

    def get_dict(self):
        return {
            "test_fullname": self.test_fullname,
            "type_doc": self.type_doc,
            "test_param_id": str(self.test_param_id),
            "amount_question": self.amount_question,
            "time": self.time
        }


class Skills_param(Base):
    __tablename__ = 'skills_param'
    type_doc = Column(String(15), nullable=False, default="skills_param")
    skill_param_id = Column(UUIDType, primary_key=True, default=uuid.uuid4)
    skills = Column(JSON, nullable=True)

    def get_dict(self):
        return {
            "type_doc": self.type_doc,
            "skill_param_id": str(self.skill_param_id),
            "skills": self.skills
        }


class Questions(Base):
    __tablename__ = 'questions'
    type_doc = Column(String(15), nullable=False, default="questions")
    question_id = Column(UUIDType, primary_key=True, default=uuid.uuid4)
    type = Column(String(30), nullable=True)
    header = Column(String(60), nullable=True)
    answers_dict = Column(JSON, nullable=True)
    true_answers_dict = Column(JSON, nullable=True)
    weight = Column(Float, nullable=True)
    skill = Column(JSON, nullable=True)

    def get_dict(self):
        return {
            "type_doc": self.type_doc,
            "question_id": str(self.question_id),
            "type": self.type,
            "header": self.header,
            "answers_dict": self.answers_dict,
            "true_answers_dict": self.true_answers_dict,
            "weight": self.weight,
            "skill": self.skill
        }
