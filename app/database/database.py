import os

from fastapi import Depends
from motor.motor_asyncio import AsyncIOMotorClient
from motor.motor_asyncio import AsyncIOMotorDatabase
from motor.motor_asyncio import AsyncIOMotorCollection
from loguru import logger
db_name = os.getenv("MONGO_INITDB_DATABASE")


def get_connect_string():
    username = os.getenv("MONGO_INITDB_ROOT_USERNAME")
    password = os.getenv("MONGO_INITDB_ROOT_PASSWORD")
    host = os.getenv("MONGO_HOST")
    host = "mongo_db"
    port = os.getenv("MONGO_PORT")
    # connection_string = f"mongodb://{username}:{password}@{host}:{port}/" Косяк в том,
    # что не используется пароль и логин
    return f"mongodb://{host}:{port}/"


mongo_client = AsyncIOMotorClient(get_connect_string())


def get_database(database_name: str = db_name):
    return mongo_client[database_name]


# получить collection_name в бд db_name  -->>>> get_collection(collection_name, get_database(db_name))
def get_collection(collection_name: str = "tests_coll", db: AsyncIOMotorDatabase = Depends(get_database)):
    return db[collection_name]
