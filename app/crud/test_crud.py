import uuid

from fastapi import Depends
from loguru import logger
from motor.motor_asyncio import AsyncIOMotorCollection

from ..database.database import get_collection


class TestParamCRUD:
    async def create_test_param(self, test_param_data: dict,
                                db: AsyncIOMotorCollection = Depends(get_collection)) -> dict:
        required_fields = ["amount_question", "time"]
        for field in required_fields:
            if field not in test_param_data or not test_param_data[field]:
                raise ValueError(f"{field} is required.")

        test_param_data["test_param_id"] = str(uuid.uuid4())
        result = await db.insert_one(test_param_data)
        return {"inserted_id": str(result.inserted_id)}

    async def read_test_param(self, db: AsyncIOMotorCollection = Depends(get_collection)) -> dict:
        result = await db.find_one({"type_doc": "test_param"})
        return result

    async def update_test_param(self, test_param_id: str, new_data: dict,
                                db: AsyncIOMotorCollection = Depends(get_collection)) -> dict:
        result = await db.update_one({"test_param_id": test_param_id}, {"$set": new_data})
        return {"modified_count": result.modified_count}

    async def delete_test_param(self, test_param_id: str, db: AsyncIOMotorCollection = Depends(get_collection)) -> dict:
        result = await db.delete_one({"test_param_id": test_param_id})
        return {"deleted_count": result.deleted_count}


class SkillsParamCRUD:
    async def create_skills_param(self, skills_param_data: dict,
                                  db: AsyncIOMotorCollection = Depends(get_collection)) -> dict:
        required_fields = ["skills"]
        for field in required_fields:
            if field not in skills_param_data or not skills_param_data[field]:
                raise ValueError(f"{field} is required.")

        skills_param_data["skill_param_id"] = str(uuid.uuid4())

        result = await db.insert_one(skills_param_data)
        return {"inserted_id": str(result.inserted_id)}

    async def read_skills_param(self, db: AsyncIOMotorCollection = Depends(get_collection)) -> dict:
        result = await db.find_one({"type_doc": "skills_param"})
        return result

    async def update_skills_param(self, skill_param_id: str, new_data: dict,
                                  db: AsyncIOMotorCollection = Depends(get_collection)) -> dict:
        result = await db.update_one({"skill_param_id": skill_param_id}, {"$set": new_data})
        return {"modified_count": result.modified_count}

    async def delete_skills_param(self, skill_param_id: str,
                                  db: AsyncIOMotorCollection = Depends(get_collection)) -> dict:
        result = await db.delete_one({"skill_param_id": skill_param_id})
        return {"deleted_count": result.deleted_count}


class QuestionsCRUD:
    async def create_question(self, question_data: dict, db: AsyncIOMotorCollection = Depends(get_collection)) -> dict:
        # Проверка наличия ключевых полей
        required_fields = ["type", "header", "answers_dict", "weight", "skill"]
        for field in required_fields:
            if field not in question_data or not question_data[field]:
                raise ValueError(f"{field} is required.")

        # Добавление дополнительных полей, если необходимо
        question_data["question_id"] = str(uuid.uuid4())

        result = await db.insert_one(question_data)
        return {"inserted_id": str(result.inserted_id)}

    async def read_question(self, question_id: str, db: AsyncIOMotorCollection = Depends(get_collection)) -> dict:
        result = await db.find_one({"question_id": question_id})
        return result

    async def read_questions_by_id(self, questions_id: list, db: AsyncIOMotorCollection = Depends(get_collection)) -> dict:
        cursor = db.find({"question_id": {"$in": questions_id}})  #Получение всех вопросов по ID
        results = await cursor.to_list(length=len(questions_id))
        return results

    async def read_questions_by_skill(self, skill: str, db: AsyncIOMotorCollection = Depends(get_collection)) -> dict:
        projection = {"question_id": 1, "skill": 1}
        cursor = db.find({"skill": skill}, projection=projection)
        result = await cursor.to_list(length=None)
        return result

    async def update_question(self, question_id: str, new_data: dict,
                              db: AsyncIOMotorCollection = Depends(get_collection)) -> dict:
        result = await db.update_one({"question_id": question_id}, {"$set": new_data})
        return {"modified_count": result.modified_count}

    async def delete_question(self, question_id: str, db: AsyncIOMotorCollection = Depends(get_collection)) -> dict:
        result = await db.delete_one({"question_id": question_id})
        return {"deleted_count": result.deleted_count}
